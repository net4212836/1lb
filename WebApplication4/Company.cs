﻿public class Company
{
    string name;
    string location;
    int numPeople;

    public Company(string name, string location, int numPeople)
    {
        this.name = name;
        this.location = location;
        this.numPeople = numPeople;
    }

    public string Name => name;
    public string Location => location;

    public override string ToString()
    {
        return base.ToString() + " name: " + name + ", location: " + location+ ", number of people: "+ numPeople;
    }
}