﻿
public class ErrorHandlingMiddleware
{
    readonly RequestDelegate next;

    public ErrorHandlingMiddleware(RequestDelegate next)
    {
        this.next = next;
    }
    public async Task InvokeAsync(HttpContext context)
    {
        Random rnd = new Random();
        int randomNumber = rnd.Next(0, 101);
        if(randomNumber < 50) {
            context.Response.StatusCode = 404;
            await context.Response.WriteAsync("You are not lucky");
        }
        else
        {
            await next.Invoke(context);
        }
    }
}
