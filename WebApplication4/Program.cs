
using Microsoft.AspNetCore.Http;

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();


app.UseMiddleware<ErrorHandlingMiddleware>();

app.MapGet("/", (context) => {
    Random rnd = new Random();
    int randomNumber = rnd.Next(0, 101);
    return context.Response.WriteAsync(new Company("BSA", "Kyiv", randomNumber).ToString());
});


app.Run();
